use serde::Deserialize;
use serde_yaml::from_str;

use crate::locale::PartiallyLocalized;

#[derive(Debug, Deserialize)]
pub struct Translations {
    pub general: General,
    pub product: Product,
    pub category: Category,
    pub vendor: Vendor,
    pub search: Search,
    pub contact: Contact,
    pub impress: Impress,
    pub privacy: Privacy,
    pub not_found: NotFound,
}

impl Translations {
    pub fn load() -> Self {
        Translations {
            general: General::load(),
            product: Product::load(),
            category: Category::load(),
            vendor: Vendor::load(),
            search: Search::load(),
            contact: Contact::load(),
            impress: Impress::load(),
            privacy: Privacy::load(),
            not_found: NotFound::load(),
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct General {
    pub brand: PartiallyLocalized<String>,
    pub slogan: PartiallyLocalized<String>,
    pub source_code: PartiallyLocalized<String>,
    pub other_resources: OtherResources,
    pub about_us: PartiallyLocalized<String>,
}

#[derive(Debug, Deserialize)]
pub struct OtherResources {
    pub name: PartiallyLocalized<String>,
    pub links: Vec<Link>,
}

#[derive(Debug, Deserialize)]
pub struct Link {
    pub label: PartiallyLocalized<String>,
    pub path: PartiallyLocalized<String>,
}

impl General {
    pub fn load() -> Self {
        from_str(include_str!("../../../data/translations/general.yaml"))
            .map_err(|e| e.to_string())
            .expect("load general.yaml")
    }
}

#[derive(Debug, Deserialize)]
pub struct Product {
    pub singular: PartiallyLocalized<String>,
    pub plural: PartiallyLocalized<String>,
    pub slug_singular: PartiallyLocalized<String>,
    pub slug_plural: PartiallyLocalized<String>,
}

impl Product {
    pub fn load() -> Self {
        from_str(include_str!("../../../data/translations/product.yaml"))
            .map_err(|e| e.to_string())
            .expect("load product.yaml")
    }
}

#[derive(Debug, Deserialize)]
pub struct Category {
    pub singular: PartiallyLocalized<String>,
    pub plural: PartiallyLocalized<String>,
    pub slug_singular: PartiallyLocalized<String>,
    pub slug_plural: PartiallyLocalized<String>,
}

impl Category {
    pub fn load() -> Self {
        from_str(include_str!("../../../data/translations/category.yaml"))
            .map_err(|e| e.to_string())
            .expect("load category.yaml")
    }
}

#[derive(Debug, Deserialize)]
pub struct Vendor {
    pub singular: PartiallyLocalized<String>,
    pub plural: PartiallyLocalized<String>,
    pub slug_singular: PartiallyLocalized<String>,
    pub slug_plural: PartiallyLocalized<String>,
}

impl Vendor {
    pub fn load() -> Self {
        from_str(include_str!("../../../data/translations/vendor.yaml"))
            .map_err(|e| e.to_string())
            .expect("load vendor.yaml")
    }
}

#[derive(Debug, Deserialize)]
pub struct Search {
    pub name: PartiallyLocalized<String>,
    pub slug: PartiallyLocalized<String>,
}

impl Search {
    pub fn load() -> Self {
        from_str(include_str!("../../../data/translations/search.yaml"))
            .map_err(|e| e.to_string())
            .expect("load search.yaml")
    }
}

#[derive(Debug, Deserialize)]
pub struct Contact {
    pub name: PartiallyLocalized<String>,
    pub slug: PartiallyLocalized<String>,
    pub email: PartiallyLocalized<String>,
}

impl Contact {
    pub fn load() -> Self {
        from_str(include_str!("../../../data/translations/contact.yaml"))
            .map_err(|e| e.to_string())
            .expect("load contact.yaml")
    }
}

#[derive(Debug, Deserialize)]
pub struct Impress {
    pub name: PartiallyLocalized<String>,
    pub slug: PartiallyLocalized<String>,
    pub text: PartiallyLocalized<String>,
    pub contact: PartiallyLocalized<String>,
    pub more_about_abineo: PartiallyLocalized<String>,
}

impl Impress {
    pub fn load() -> Self {
        from_str(include_str!("../../../data/translations/impress.yaml"))
            .map_err(|e| e.to_string())
            .expect("load impress.yaml")
    }
}

#[derive(Debug, Deserialize)]
pub struct Privacy {
    pub name: PartiallyLocalized<String>,
    pub slug: PartiallyLocalized<String>,
    pub text: PartiallyLocalized<String>,
    pub contact: PartiallyLocalized<String>,
}

impl Privacy {
    pub fn load() -> Self {
        from_str(include_str!("../../../data/translations/privacy.yaml"))
            .map_err(|e| e.to_string())
            .expect("load privacy.yaml")
    }
}

#[derive(Debug, Deserialize)]
pub struct NotFound {
    pub name: PartiallyLocalized<String>,
    pub text: PartiallyLocalized<String>,
    pub search_results: PartiallyLocalized<String>,
}

impl NotFound {
    pub fn load() -> Self {
        from_str(include_str!("../../../data/translations/not_found.yaml"))
            .map_err(|e| e.to_string())
            .expect("load not_found.yaml")
    }
}
