use crate::routes::Route;
use parser::data::{CategoryMap, ProductMap, VendorMap};
use parser::translations::Translations;

pub fn render_categories_overview(
    translations: &Translations,
    products: &ProductMap,
    categories: &CategoryMap,
    vendors: &VendorMap,
) -> Option<Route> {
    None
}

pub fn render_category_detail(
    translations: &Translations,
    id: &String,
    products: &ProductMap,
    categories: &CategoryMap,
    vendors: &VendorMap,
) -> Option<Route> {
    None
}
