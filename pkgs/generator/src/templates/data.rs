use std::path::Path;

use askama::Template;
use chrono::{DateTime, Utc};

use parser::locale::Locale;

use crate::routes::Route;
use crate::write::write_file;

#[derive(Template)]
#[template(path = "robots.txt")]
pub struct RobotsTxt;

pub fn render_robot() -> Option<Route> {
    let data = RobotsTxt.to_string();
    write_file(Path::new("robots.txt"), data);
    None
}

#[derive(Template)]
#[template(path = "opensearch.xml")]
pub struct OpenSearch {
    locales: [Locale; Locale::LOCALES],
}

impl OpenSearch {
    pub fn new() -> Self {
        Self {
            locales: Locale::ALL,
        }
    }
}

pub fn render_opensearch() -> Option<Route> {
    let data = OpenSearch::new().to_string();
    write_file(Path::new("opensearch.xml"), data);
    None
}

#[derive(Template)]
#[template(path = "sitemap.xml")]
pub struct Sitemap {
    urls: Vec<Url>,
}

impl Sitemap {
    pub fn from_routes(routes: &Vec<Route>) -> Self {
        let urls = routes.into_iter().map(From::from).collect();
        Self { urls }
    }
}

struct Url {
    loc: String,
    lastmod: Option<DateTime<Utc>>,
    alternates: Vec<Alternate>,
}

impl From<&Route> for Url {
    fn from(route: &Route) -> Self {
        Url {
            loc: route.to_pathname(None),
            lastmod: route.updated_at,
            alternates: Locale::ALL
                .into_iter()
                .map(|locale| Alternate::new(locale, route))
                .collect(),
        }
    }
}

struct Alternate {
    lang: String,
    href: String,
}

impl Alternate {
    fn new(locale: Locale, route: &Route) -> Self {
        Alternate {
            lang: locale.as_iso().to_string(),
            href: route.to_pathname(Some(locale)),
        }
    }
}

pub fn render_sitemap(routes: &Vec<Route>) {
    let data = Sitemap::from_routes(routes).to_string();
    write_file(Path::new("sitemap.xml"), data);
}
