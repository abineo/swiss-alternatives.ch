use std::fs::{read_dir, read_to_string};
use std::ops::Deref;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::sync::OnceLock;

use chrono::{DateTime, Utc};
use dashmap::DashMap;
use rayon::prelude::*;
use serde::Deserialize;
use serde_yaml::from_str;

use crate::locale::PartiallyLocalized;

fn base_dir() -> &'static Path {
    static CELL: OnceLock<PathBuf> = OnceLock::new();
    CELL.get_or_init(|| {
        let stdout = Command::new(env!("CARGO"))
            .args(["locate-project", "--workspace", "--message-format=plain"])
            .output()
            .unwrap()
            .stdout;
        std::str::from_utf8(&stdout)
            .unwrap()
            .trim()
            .strip_suffix("Cargo.toml")
            .map(Path::new)
            .unwrap()
            .join("data")
    })
}

#[derive(Debug, Deserialize)]
pub struct Product {
    pub name: String,
    pub description: PartiallyLocalized<String>,
    pub vendors: Vec<String>,
    pub categories: Vec<String>,
    pub logo: String,
    #[serde(default)]
    pub images: Vec<String>,
    pub created_at: DateTime<Utc>,
    #[serde(default)]
    pub updated_at: Option<DateTime<Utc>>,
}

#[derive(Debug)]
pub struct ProductMap {
    map: DashMap<String, Product>,
}

impl ProductMap {
    pub fn load() -> Self {
        let map = DashMap::new();
        let dir = base_dir().join("products");
        read_dir(&dir)
            .map_err(|e| e.to_string())
            .expect("scan dir `data/products`")
            .map(|rd| {
                rd.map_err(|e| e.to_string())
                    .expect("read dir entry in `data/products`")
                    .file_name()
                    .into_string()
                    .map_err(|e| e.to_string_lossy().to_string())
                    .expect("product filename")
            })
            .collect::<Vec<String>>()
            .into_par_iter()
            .for_each(|filename| {
                let yaml = read_to_string(dir.join(&filename))
                    .map_err(|e| format!("{filename}: {e}"))
                    .expect("read product file");
                let product = from_str(&yaml)
                    .map_err(|e| format!("{filename}: {e}"))
                    .expect("parse product file");
                map.insert(
                    filename
                        .strip_suffix(".yaml")
                        .ok_or(&filename)
                        .expect("product filename ends with .yaml")
                        .to_string(),
                    product,
                );
            });
        Self { map }
    }
}

impl Deref for ProductMap {
    type Target = DashMap<String, Product>;

    fn deref(&self) -> &Self::Target {
        &self.map
    }
}

#[derive(Debug, Deserialize)]
pub struct Category {
    pub name: PartiallyLocalized<String>,
    pub description: PartiallyLocalized<String>,
    pub path: PartiallyLocalized<String>,
    pub icon: String,
    #[serde(default)]
    pub products: Vec<String>,
    #[serde(default)]
    pub vendors: Vec<String>,
}

#[derive(Debug)]
pub struct CategoryMap {
    map: DashMap<String, Category>,
}

impl CategoryMap {
    pub fn load() -> Self {
        let map = DashMap::new();
        let dir = base_dir().join("categories");
        read_dir(&dir)
            .map_err(|e| e.to_string())
            .expect("scan dir `data/categories`")
            .map(|rd| {
                rd.map_err(|e| e.to_string())
                    .expect("read dir entry in `data/categories`")
                    .file_name()
                    .into_string()
                    .map_err(|e| e.to_string_lossy().to_string())
                    .expect("category filename")
            })
            .collect::<Vec<String>>()
            .into_par_iter()
            .for_each(|filename| {
                let yaml = read_to_string(dir.join(&filename))
                    .map_err(|e| format!("{filename}: {e}"))
                    .expect("read category file");
                let category = from_str(&yaml)
                    .map_err(|e| format!("{filename}: {e}"))
                    .expect("parse category file");
                map.insert(
                    filename
                        .strip_suffix(".yaml")
                        .ok_or(&filename)
                        .expect("category filename ends with .yaml")
                        .to_string(),
                    category,
                );
            });
        Self { map }
    }

    pub fn populate_backlinks(&self, products: &ProductMap) {
        products.par_iter().for_each(|p| {
            p.categories.iter().for_each(|c| {
                if let Some(mut category) = self.get_mut(c) {
                    category.products.push(p.key().to_string());
                    category.vendors.append(&mut p.vendors.clone());
                }
            })
        });
    }
}

impl Deref for CategoryMap {
    type Target = DashMap<String, Category>;

    fn deref(&self) -> &Self::Target {
        &self.map
    }
}

#[derive(Debug, Deserialize)]
pub struct Vendor {
    pub name: String,
    pub full_name: String,
    pub description: PartiallyLocalized<String>,
    pub logo: String,
    #[serde(default)]
    pub products: Vec<String>,
    #[serde(default)]
    pub categories: Vec<String>,
}

#[derive(Debug)]
pub struct VendorMap {
    map: DashMap<String, Vendor>,
}

impl VendorMap {
    pub fn load() -> Self {
        let map = DashMap::new();
        let dir = base_dir().join("vendors");
        read_dir(&dir)
            .map_err(|e| e.to_string())
            .expect("scan dir `data/vendors`")
            .map(|rd| {
                rd.map_err(|e| e.to_string())
                    .expect("read dir entry in `data/vendors`")
                    .file_name()
                    .into_string()
                    .map_err(|e| e.to_string_lossy().to_string())
                    .expect("vendor filename")
            })
            .collect::<Vec<String>>()
            .into_par_iter()
            .for_each(|filename| {
                let yaml = read_to_string(dir.join(&filename))
                    .map_err(|e| format!("{filename}: {e}"))
                    .expect("read vendor file");
                let vendor = from_str(&yaml)
                    .map_err(|e| format!("{filename}: {e}"))
                    .expect("parse vendor file");
                map.insert(
                    filename
                        .strip_suffix(".yaml")
                        .ok_or(&filename)
                        .expect("vendor filename ends with .yaml")
                        .to_string(),
                    vendor,
                );
            });
        Self { map }
    }

    pub fn populate_backlinks(&self, products: &ProductMap) {
        products.par_iter().for_each(|p| {
            p.vendors.iter().for_each(|v| {
                if let Some(mut vendor) = self.get_mut(v) {
                    vendor.products.push(p.key().to_string());
                    vendor.categories.append(&mut p.categories.clone());
                }
            })
        });
    }
}

impl Deref for VendorMap {
    type Target = DashMap<String, Vendor>;

    fn deref(&self) -> &Self::Target {
        &self.map
    }
}
