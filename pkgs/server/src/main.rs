use std::path::PathBuf;
use std::time::Instant;

use axum::{
    extract::Request,
    http::{header::SERVER, HeaderValue},
    middleware::{from_fn, Next},
    response::Response,
    Router,
};
use config::{Config, Environment};
use humantime::format_duration;
use serde::Deserialize;
use tokio::signal;
use tower::ServiceBuilder;
use tower_http::normalize_path::NormalizePathLayer;
use tower_http::{
    compression::CompressionLayer, set_header::SetResponseHeaderLayer, trace::TraceLayer,
};
use tracing::{info, Level};

use crate::routes::fallback_handler;

mod routes;

#[derive(Clone, Debug, Deserialize)]
pub struct AppConfig {
    pub port: u16,
    pub static_dir: PathBuf,
    pub dist_dir: PathBuf,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt()
        .with_max_level(Level::DEBUG)
        .init();

    info!("Hello");

    let cfg: AppConfig = Config::builder()
        .set_default("port", 7441)?
        .set_default("static_dir", "static")?
        .set_default("dist_dir", "dist")?
        .add_source(Environment::with_prefix("app"))
        .build()?
        .try_deserialize()?;

    let listener = tokio::net::TcpListener::bind(("0.0.0.0", cfg.port)).await?;
    info!("listening on http://localhost:{}", cfg.port);

    info!(
        "serving static files from {}",
        cfg.static_dir.to_str().unwrap()
    );

    axum::serve(listener, make_service(cfg))
        .with_graceful_shutdown(shutdown_signal())
        .await?;

    info!("Bye");

    Ok(())
}

fn make_service(cfg: AppConfig) -> Router {
    let server_value = HeaderValue::from_static("Axum (Hyper)");
    let server_header = SetResponseHeaderLayer::overriding(SERVER, server_value);
    Router::new()
        .merge(routes::static_files(&cfg))
        .merge(routes::dynamic_files(&cfg))
        .layer(
            ServiceBuilder::new()
                .layer(from_fn(response_time))
                .layer(TraceLayer::new_for_http())
                .layer(server_header)
                .layer(CompressionLayer::new().br(true))
                .layer(NormalizePathLayer::trim_trailing_slash()),
        )
        .fallback(fallback_handler)
        .with_state(cfg)
}

async fn response_time(request: Request, next: Next) -> Response {
    let start = Instant::now();
    let mut response = next.run(request).await;
    let duration = start.elapsed();
    if let Ok(value) = HeaderValue::from_str(&format_duration(duration).to_string()) {
        response.headers_mut().insert("x-response-time", value);
    }
    response
}

async fn shutdown_signal() {
    let ctrl_c = async {
        signal::ctrl_c()
            .await
            .expect("failed to install Ctrl+C handler");
    };

    #[cfg(unix)]
    let terminate = async {
        signal::unix::signal(signal::unix::SignalKind::terminate())
            .expect("failed to install signal handler")
            .recv()
            .await;
    };

    #[cfg(not(unix))]
    let terminate = std::future::pending::<()>();

    tokio::select! {
        _ = ctrl_c => info!("received SIGINT"),
        _ = terminate => info!("received SIGTERM"),
    }
}
