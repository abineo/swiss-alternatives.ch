use std::path::PathBuf;

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use parser::locale::{Locale, Localized};

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Route {
    pub canonical: Vec<String>,
    pub localized: Localized<Vec<String>>,
    pub updated_at: Option<DateTime<Utc>>,
}

impl Route {
    pub fn to_pathname(&self, locale: Option<Locale>) -> String {
        match locale {
            None => {
                format!("/{}", self.localized[Locale::default()].join("/"))
            }
            Some(locale) => {
                let parts = &self.localized[locale];
                if parts.is_empty() {
                    format!("/{}", locale.as_slug())
                } else {
                    format!("/{}/{}", locale.as_slug(), parts.join("/"))
                }
            }
        }
    }

    pub fn to_pathbuf(&self, locale: Locale) -> PathBuf {
        let mut path = PathBuf::from(locale.as_slug());
        for part in &self.localized[locale] {
            path.push(part);
        }
        path
    }
}
