use std::fs::read_dir;
use std::path::PathBuf;
use std::sync::Arc;

use axum::body::Body;
use axum::extract::{FromRequestParts, OriginalUri, State};
use axum::http::header::{CACHE_CONTROL, CONTENT_TYPE, VARY};
use axum::http::request::Parts;
use axum::response::Redirect;
use axum::{
    async_trait,
    http::{header::ACCEPT_LANGUAGE, HeaderMap, HeaderValue},
    response::IntoResponse,
    routing::get,
    Extension, Router,
};
use bincode::deserialize;
use tokio::fs::File;
use tokio_util::io::ReaderStream;
use tower_http::services::ServeFile;
use tower_http::{services::ServeDir, set_header::SetResponseHeaderLayer};

use generator::routes::Route;
use parser::locale::Locale;

use crate::AppConfig;

pub fn static_files(cfg: &AppConfig) -> Router<AppConfig> {
    let mut router = Router::new()
        .route(
            "/favicon.ico",
            get(|| async { Redirect::permanent("/media/swiss-alternatives.ch/figurative.svg") }),
        )
        .nest_service(
            "/robots.txt",
            ServeFile::new(cfg.dist_dir.join("robots.txt")),
        )
        .nest_service(
            "/opensearch.xml",
            ServeFile::new(cfg.dist_dir.join("opensearch.xml")),
        );

    for dir in read_dir(&cfg.static_dir)
        .expect("read static dir")
        .filter_map(Result::ok)
        .filter(|entry| entry.path().is_dir())
        .filter_map(|d| d.file_name().to_str().map(ToString::to_string))
    {
        router = router.nest_service(&format!("/{dir}"), ServeDir::new(cfg.static_dir.join(dir)));
    }

    with_caching(router)
}

pub fn dynamic_files(cfg: &AppConfig) -> Router<AppConfig> {
    let router = Router::new()
        .nest_service(
            "/sitemap.xml",
            ServeFile::new(cfg.dist_dir.join("sitemap.xml")),
        )
        .merge(from_generator(cfg).layer(SetResponseHeaderLayer::appending(
            VARY,
            HeaderValue::from_static("hx-request"),
        )));
    with_caching(router)
}

#[cfg(not(debug_assertions))]
fn with_caching(router: Router<AppConfig>) -> Router<AppConfig> {
    router.layer(SetResponseHeaderLayer::overriding(
        CACHE_CONTROL,
        HeaderValue::from_static(
            "public, stale-while-revalidate, max-age=86400", /* 1 day */
        ),
    ))
}

#[cfg(debug_assertions)]
fn with_caching(router: Router<AppConfig>) -> Router<AppConfig> {
    router.layer(SetResponseHeaderLayer::overriding(
        CACHE_CONTROL,
        HeaderValue::from_static("no-cache"),
    ))
}

pub fn from_generator(cfg: &AppConfig) -> Router<AppConfig> {
    let routes_bin = include_bytes!("../../../dist/routes.bin");
    let routes: Vec<Route> = deserialize(routes_bin).expect("parse routes.bin");

    let vary_accept_language_header =
        SetResponseHeaderLayer::appending(VARY, HeaderValue::from_static("accept-language"));

    let mut router = Router::new();
    for route in routes {
        let canonical_router = Router::new()
            .route(route.to_pathname(None).as_str(), get(canonical_handler))
            .layer(Extension(Arc::new(route.clone())))
            .layer(vary_accept_language_header.clone());
        router = router.merge(canonical_router);

        for locale in Locale::ALL {
            let localized_router = Router::new()
                .route(
                    route.to_pathname(Some(locale)).as_str(),
                    get(localized_handler),
                )
                .layer(Extension(Arc::new(
                    cfg.dist_dir.join(route.to_pathbuf(locale)),
                )));
            router = router.merge(localized_router);
        }
    }
    router
}

async fn canonical_handler(
    State(cfg): State<AppConfig>,
    Extension(route): Extension<Arc<Route>>,
    PrefLocale(locale): PrefLocale,
    Partial(partial): Partial,
) -> impl IntoResponse {
    let path = cfg.dist_dir.join(route.to_pathbuf(locale));
    match partial {
        true => stream_file(path.join("partial.html")).await,
        false => stream_file(path.join("index.html")).await,
    }
}

async fn localized_handler(
    Extension(path): Extension<Arc<PathBuf>>,
    Partial(partial): Partial,
) -> impl IntoResponse {
    match partial {
        true => stream_file(path.join("partial.html")).await,
        false => stream_file(path.join("index.html")).await,
    }
}

async fn stream_file(path: PathBuf) -> impl IntoResponse {
    let file = File::open(&path).await.unwrap();
    let mut headers = HeaderMap::new();
    let content_type = HeaderValue::from_static("text/html; charset=utf-8");
    headers.insert(CONTENT_TYPE, content_type);
    let body = Body::from_stream(ReaderStream::new(file));
    (headers, body)
}

pub async fn fallback_handler(
    State(cfg): State<AppConfig>,
    PrefLocale(locale): PrefLocale,
    Partial(partial): Partial,
    OriginalUri(uri): OriginalUri,
) -> impl IntoResponse {
    let path = uri.path().strip_prefix('/').unwrap_or(uri.path());
    let locale = match path.split('/').next() {
        Some(lang) => Locale::try_from(lang).unwrap_or(locale),
        None => locale,
    };
    let base = cfg.dist_dir.join(locale.as_slug()).join("not_found");
    match partial {
        true => stream_file(base.join("partial.html")).await,
        false => stream_file(base.join("index.html")).await,
    }
}

#[derive(Debug, Clone)]
pub struct Partial(pub bool);

#[async_trait]
impl<S> FromRequestParts<S> for Partial
where
    S: Send + Sync,
{
    type Rejection = ();

    async fn from_request_parts(parts: &mut Parts, _: &S) -> Result<Self, Self::Rejection> {
        Ok(Partial(parts.headers.contains_key("hx-request")))
    }
}

#[derive(Debug, Clone)]
pub struct PrefLocale(pub Locale);

#[async_trait]
impl<S> FromRequestParts<S> for PrefLocale
where
    S: Send + Sync,
{
    type Rejection = ();

    async fn from_request_parts(parts: &mut Parts, _: &S) -> Result<Self, Self::Rejection> {
        Ok(PrefLocale(parse_header(&parts.headers).unwrap_or_default()))
    }
}

fn parse_header(headers: &HeaderMap) -> Option<Locale> {
    Locale::try_from(headers.get(ACCEPT_LANGUAGE)?.to_str().ok()?)
}
