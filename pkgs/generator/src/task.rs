use crate::routes::Route;
use crate::templates::categories::{render_categories_overview, render_category_detail};
use crate::templates::contact::render_contact;
use crate::templates::data::{render_opensearch, render_robot};
use crate::templates::home::render_home;
use crate::templates::impress::render_impress;
use crate::templates::not_found::render_not_found;
use crate::templates::privacy::render_privacy;
use crate::templates::products::{render_product_detail, render_products_overview};
use crate::templates::vendors::{render_vendor_detail, render_vendors_overview};
use parser::data::{CategoryMap, ProductMap, VendorMap};
use parser::translations::Translations;

pub enum Task<'a> {
    Home {
        translations: &'a Translations,
        products: &'a ProductMap,
        categories: &'a CategoryMap,
        vendors: &'a VendorMap,
    },
    CategoriesOverview {
        translations: &'a Translations,
        products: &'a ProductMap,
        categories: &'a CategoryMap,
        vendors: &'a VendorMap,
    },
    CategoryDetail {
        translations: &'a Translations,
        id: String,
        products: &'a ProductMap,
        categories: &'a CategoryMap,
        vendors: &'a VendorMap,
    },
    ProductsOverview {
        translations: &'a Translations,
        products: &'a ProductMap,
        categories: &'a CategoryMap,
        vendors: &'a VendorMap,
    },
    ProductDetail {
        translations: &'a Translations,
        id: String,
        products: &'a ProductMap,
        categories: &'a CategoryMap,
        vendors: &'a VendorMap,
    },
    VendorsOverview {
        translations: &'a Translations,
        products: &'a ProductMap,
        categories: &'a CategoryMap,
        vendors: &'a VendorMap,
    },
    VendorDetail {
        translations: &'a Translations,
        id: String,
        products: &'a ProductMap,
        categories: &'a CategoryMap,
        vendors: &'a VendorMap,
    },
    Contact {
        translations: &'a Translations,
    },
    Impress {
        translations: &'a Translations,
    },
    Privacy {
        translations: &'a Translations,
    },
    NotFound {
        translations: &'a Translations,
    },
    Robots,
    OpenSearch,
}

impl<'a> Task<'a> {
    pub fn run(&self) -> Option<Route> {
        match self {
            Task::Home {
                translations,
                products,
                categories,
                vendors,
            } => render_home(translations, products, categories, vendors),
            Task::CategoriesOverview {
                translations,
                products,
                categories,
                vendors,
            } => render_categories_overview(translations, products, categories, vendors),
            Task::CategoryDetail {
                translations,
                id,
                products,
                categories,
                vendors,
            } => render_category_detail(translations, id, products, categories, vendors),
            Task::ProductsOverview {
                translations,
                products,
                categories,
                vendors,
            } => render_products_overview(translations, products, categories, vendors),
            Task::ProductDetail {
                translations,
                id,
                products,
                categories,
                vendors,
            } => render_product_detail(translations, id, products, categories, vendors),
            Task::VendorsOverview {
                translations,
                products,
                categories,
                vendors,
            } => render_vendors_overview(translations, products, categories, vendors),
            Task::VendorDetail {
                translations,
                id,
                products,
                categories,
                vendors,
            } => render_vendor_detail(translations, id, products, categories, vendors),
            Task::Contact { translations } => render_contact(translations),
            Task::Impress { translations } => render_impress(translations),
            Task::Privacy { translations } => render_privacy(translations),
            Task::NotFound { translations } => render_not_found(translations),
            Task::Robots => render_robot(),
            Task::OpenSearch => render_opensearch(),
        }
    }
}
