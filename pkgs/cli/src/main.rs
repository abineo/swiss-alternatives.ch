use chrono::{SecondsFormat, Utc};
use clap::{Parser, Subcommand};

#[derive(Parser)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// prints the current time as RFC 3339 / ISO 8601 datetime string
    #[command(visible_alias = "ts")]
    Timestamp,
}

fn main() {
    let cli = Cli::parse();

    match &cli.command {
        Commands::Timestamp => {
            println!("{}", Utc::now().to_rfc3339_opts(SecondsFormat::Secs, true));
        }
    }
}
