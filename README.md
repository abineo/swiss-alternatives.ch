# [swiss-alternatives.ch](https://www.swiss-alternatives.ch/)

[![Website](https://img.shields.io/website?url=https%3A%2F%2Fswiss-alternatives.ch)](https://www.swiss-alternatives.ch/)
[![Pipeline](https://gitlab.com/abineo/swiss-alternatives.ch/badges/next/pipeline.svg)](https://gitlab.com/abineo/swiss-alternatives.ch/-/pipelines)
[![Latest Release](https://gitlab.com/abineo/swiss-alternatives.ch/-/badges/release.svg)](https://gitlab.com/abineo/swiss-alternatives.ch/-/releases)

## License

[Source available](./LICENSE)

## Project structure

- `/static` Static assets like fonts and logos.
- ... TODO

## Developer Setup

Make sure you have the [rust toolchain](https://rustup.rs/) and [pnpm](https://pnpm.io/installation) installed.

Install dependencies from npm with:

```sh
pnpm install
```

Start the server with:

```sh
cargo run
```

If you have [cargo-watch](https://github.com/watchexec/cargo-watch#install) installed, you can also run:

```sh
cargo dev
```

Open [localhost:7441](http://localhost:7441).

### Cli

There is a cli with some helper functions.  
To see available commands run:

```sh
cargo cli
```
