FROM docker.io/library/debian:bookworm AS passwd
RUN mkdir --parents /export/etc && \
    echo "server:x:10001:10001:Server:/var/server:/bin/false" > /export/etc/passwd && \
    mkdir --parents /export/bin && \
    mv /bin/false /export/bin

FROM docker.io/library/node:22-bookworm AS deps
RUN npm install --global @pnpm/exe && \
    mkdir --parents /export/bin && \
    mv $(realpath $(which pnpm)) /export/bin && \
    mv $(realpath $(which node)) /export/bin

FROM docker.io/messense/rust-musl-cross:x86_64-musl AS build
WORKDIR /app
RUN mkdir --parents /export/bin && \
    mkdir --parents /export/var
COPY --from=deps --chmod=555 /export/bin/* /bin
COPY . .
RUN cd /app/pkgs/generator && \
    pnpm install && \
    rm /app/static/scripts/* && \
    cp node_modules/alpinejs/dist/cdn.min.js /app/static/scripts/alpine.min.js && \
    cp node_modules/htmx.org/dist/htmx.min.js /app/static/scripts/htmx.min.js && \
    cd /app && \
    rustup target add x86_64-unknown-linux-musl && \
    cargo build --release --target=x86_64-unknown-linux-musl && \
    mv /app/target/x86_64-unknown-linux-musl/release/server /export/bin && \
    mv /app/static /export/var && \
    mv /app/dist /export/var

FROM scratch AS run
USER server
WORKDIR /var/server
COPY --from=passwd /export/etc /etc
COPY --from=passwd /export/bin /bin
COPY --from=build /export/bin /bin
COPY --from=build /export/var /var/server
ENTRYPOINT ["/bin/server"]
