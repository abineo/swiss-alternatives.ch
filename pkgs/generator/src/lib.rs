use std::path::Path;
use std::time::Instant;

use bincode::serialize;
use chrono::{SecondsFormat, Utc};
use humansize::{format_size, BINARY};
use rayon::prelude::*;

use parser::data::{CategoryMap, ProductMap, VendorMap};
use parser::translations::Translations;
use task::Task;

use crate::routes::Route;
use crate::templates::data::render_sitemap;
use crate::write::{clear_dir, write_file};

mod meta;
pub mod routes;
mod task;
mod templates;
mod write;

const ORIGIN: &str = "https://www.swiss-alternatives.ch";

fn now() -> String {
    Utc::now().to_rfc3339_opts(SecondsFormat::Millis, true)
}

pub fn format_title(parts: Vec<&str>) -> String {
    parts.join(" | ")
}

#[allow(clippy::identity_op)]
pub fn run() {
    let ts_start = Instant::now();

    let translations = Translations::load();
    let products = ProductMap::load();
    let categories = CategoryMap::load();
    let vendors = VendorMap::load();
    categories.populate_backlinks(&products);
    vendors.populate_backlinks(&products);
    dbg!(&categories, &vendors);
    println!(
        "generator: loaded translations, {} products, {} categories and {} vendors in {:?}",
        products.len(),
        categories.len(),
        vendors.len(),
        ts_start.elapsed()
    );

    let ts = Instant::now();
    let len = 0
		+ 1 // home
		+ 1 // contact
		+ 1 // impress
		+ 1 // privacy
		+ 1 // not found
		+ 1 // robots
		+ 1 // opensearch
		+ products.len() * 2
		+ categories.len() * 2
		+ vendors.len() * 2;
    let mut tasks = Vec::with_capacity(len);

    tasks.push(Task::Home {
        translations: &translations,
        products: &products,
        categories: &categories,
        vendors: &vendors,
    });
    products.iter().for_each(|p| {
        tasks.push(Task::ProductsOverview {
            translations: &translations,
            products: &products,
            categories: &categories,
            vendors: &vendors,
        });
        tasks.push(Task::ProductDetail {
            translations: &translations,
            id: p.key().to_string(),
            products: &products,
            categories: &categories,
            vendors: &vendors,
        });
    });
    categories.iter().for_each(|c| {
        tasks.push(Task::CategoriesOverview {
            translations: &translations,
            products: &products,
            categories: &categories,
            vendors: &vendors,
        });
        tasks.push(Task::CategoryDetail {
            translations: &translations,
            id: c.key().to_string(),
            products: &products,
            categories: &categories,
            vendors: &vendors,
        });
    });
    vendors.iter().for_each(|v| {
        tasks.push(Task::VendorsOverview {
            translations: &translations,
            products: &products,
            categories: &categories,
            vendors: &vendors,
        });
        tasks.push(Task::VendorDetail {
            translations: &translations,
            id: v.key().to_string(),
            products: &products,
            categories: &categories,
            vendors: &vendors,
        });
    });
    tasks.push(Task::Contact {
        translations: &translations,
    });
    tasks.push(Task::Impress {
        translations: &translations,
    });
    tasks.push(Task::Privacy {
        translations: &translations,
    });
    tasks.push(Task::NotFound {
        translations: &translations,
    });
    tasks.push(Task::Robots);
    tasks.push(Task::OpenSearch);

    if tasks.len() == len {
        println!("generator: created {len} tasks in {:?}", ts.elapsed());
    } else {
        println!(
            "generator: created {} tasks in {:?} (estimated to be {len})",
            tasks.len(),
            ts.elapsed()
        );
    }

    let ts = Instant::now();
    clear_dir();
    let routes: Vec<Route> = tasks.par_iter().filter_map(Task::run).collect();
    println!("generator: tasks took {:?} to run", ts.elapsed());
    drop(tasks);

    let ts = Instant::now();
    render_sitemap(&routes);
    println!("generator: sitemap took {:?} to render", ts.elapsed());

    let ts = Instant::now();
    let data = serialize(&routes)
        .map_err(|e| e.to_string())
        .expect("serialize routes");
    let size = data.len();
    write_file(Path::new("routes.bin"), data);
    println!(
        "generator: serialized {} routes in {:?} {}",
        routes.len(),
        ts.elapsed(),
        format_size(size, BINARY)
    );

    println!("generator: total build time was {:?}", ts_start.elapsed());
}
