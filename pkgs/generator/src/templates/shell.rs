use crate::meta::MetaData;
use askama::Template;
use parser::data::Product;

#[derive(Template)]
#[template(path = "shell.html")]
pub struct Shell<'a> {
    pub meta: &'a MetaData<'a>,
    pub inner: &'a String,
}

impl<'a> Shell<'a> {
    pub fn new(meta: &'a MetaData, inner: &'a String) -> Self {
        Self { meta, inner }
    }
}

#[derive(Template)]
#[template(path = "product.shell.html")]
pub struct ProductShell<'a> {
    pub meta: &'a MetaData<'a>,
    pub inner: &'a String,
    pub product: &'a Product,
    pub created_at: &'a str,
    pub updated_at: &'a str,
}

impl<'a> ProductShell<'a> {
    pub fn new(
        meta: &'a MetaData<'a>,
        inner: &'a String,
        product: &'a Product,
        created_at: &'a str,
        updated_at: &'a str,
    ) -> Self {
        Self {
            meta,
            inner,
            product,
            created_at,
            updated_at,
        }
    }
}
