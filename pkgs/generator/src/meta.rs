use parser::locale::{Locale, Localized};

pub struct MetaData<'a> {
    pub locale: Locale,
    pub title: &'a String,
    pub description: &'a String,
    pub canonical_path: &'a String,
    pub paths: &'a Localized<&'a String>,
}
