use http::header::ACCEPT_LANGUAGE;
use http::{HeaderMap, HeaderValue};
use serde::{Deserialize, Serialize};
use std::fmt::{Debug, Display, Formatter};
use std::ops::Index;

#[derive(Deserialize, Default, Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Locale {
    #[default]
    #[serde(rename = "en")]
    English,
    #[serde(rename = "de")]
    German,
    #[serde(rename = "fr")]
    French,
    #[serde(rename = "it")]
    Italian,
    #[serde(rename = "gsw")]
    SwissGerman,
}

impl Locale {
    pub const LOCALES: usize = 5;

    pub const ALL: [Locale; Locale::LOCALES] = [
        Locale::English,
        Locale::German,
        Locale::French,
        Locale::Italian,
        Locale::SwissGerman,
    ];

    pub fn as_name(&self) -> &'static str {
        match self {
            Self::English => "English",
            Self::German => "Deutsch",
            Self::French => "Français",
            Self::Italian => "Italiano",
            Self::SwissGerman => "Schwyzerdütsch",
        }
    }

    pub fn as_iso(&self) -> &'static str {
        match self {
            Self::English => "en-CH",
            Self::German => "de-CH",
            Self::French => "fr-CH",
            Self::Italian => "it-CH",
            Self::SwissGerman => "gsw",
        }
    }

    pub fn as_slug(&self) -> &'static str {
        match self {
            Self::English => "en",
            Self::German => "de",
            Self::French => "fr",
            Self::Italian => "it",
            Self::SwissGerman => "gsw",
        }
    }

    pub fn try_from(code: &str) -> Option<Self> {
        if code.starts_with("en") {
            return Some(Self::English);
        }
        if code.starts_with("en") {
            return Some(Self::English);
        }
        if code.starts_with("de") {
            return Some(Self::German);
        }
        if code.starts_with("fr") {
            return Some(Self::French);
        }
        if code.starts_with("it") {
            return Some(Self::Italian);
        }
        if code.starts_with("gsw") {
            return Some(Self::SwissGerman);
        }
        None
    }

    pub const fn others(&self) -> [Locale; Locale::LOCALES - 1] {
        match self {
            Self::English => [
                Locale::German,
                Locale::French,
                Locale::Italian,
                Locale::SwissGerman,
            ],
            Self::German => [
                Locale::English,
                Locale::French,
                Locale::Italian,
                Locale::SwissGerman,
            ],
            Self::French => [
                Locale::English,
                Locale::German,
                Locale::Italian,
                Locale::SwissGerman,
            ],
            Self::Italian => [
                Locale::English,
                Locale::German,
                Locale::French,
                Locale::SwissGerman,
            ],
            Self::SwissGerman => [
                Locale::English,
                Locale::German,
                Locale::French,
                Locale::Italian,
            ],
        }
    }
}

impl Display for Locale {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.as_name())
    }
}

impl From<&HeaderMap> for Locale {
    fn from(headers: &HeaderMap) -> Self {
        headers
            .get(ACCEPT_LANGUAGE)
            .map(From::from)
            .unwrap_or_default()
    }
}

impl From<&HeaderValue> for Locale {
    fn from(header: &HeaderValue) -> Self {
        header.to_str().map(From::from).unwrap_or_default()
    }
}

impl From<&str> for Locale {
    fn from(value: &str) -> Self {
        value
            .split(',')
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .filter_map(Self::try_from)
            .next() // not sorting by `q` for simplicity
            .unwrap_or_default()
    }
}

impl From<&String> for Locale {
    fn from(value: &String) -> Self {
        Locale::from(value.as_str())
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Localized<T> {
    pub en: T,
    pub de: T,
    pub fr: T,
    pub it: T,
    pub gsw: T,
}

impl<T> Localized<T> {
    pub fn new(en: T, de: T, fr: T, it: T, gsw: T) -> Self {
        Self {
            en,
            de,
            fr,
            it,
            gsw,
        }
    }

    pub fn new_with(builder: impl Fn(Locale) -> T) -> Self {
        Self {
            en: builder(Locale::English),
            de: builder(Locale::German),
            fr: builder(Locale::French),
            it: builder(Locale::Italian),
            gsw: builder(Locale::SwissGerman),
        }
    }

    pub fn as_ref(&self) -> Localized<&T> {
        Localized {
            en: &self.en,
            de: &self.de,
            fr: &self.fr,
            it: &self.it,
            gsw: &self.gsw,
        }
    }

    pub fn values(&self) -> Vec<(Locale, &T)> {
        Locale::ALL
            .into_iter()
            .map(|loc| (loc, &self[loc]))
            .collect()
    }

    pub fn values_filtered(&self, locale: &Locale) -> Vec<(Locale, &T)> {
        Locale::ALL
            .into_iter()
            .filter_map(|loc| {
                if locale == &loc {
                    return None;
                }
                Some((loc, &self[loc]))
            })
            .collect()
    }
}

impl<T> Index<Locale> for Localized<T> {
    type Output = T;

    fn index(&self, index: Locale) -> &Self::Output {
        match index {
            Locale::English => &self.en,
            Locale::German => &self.de,
            Locale::French => &self.fr,
            Locale::Italian => &self.it,
            Locale::SwissGerman => &self.gsw,
        }
    }
}

impl Localized<String> {
    pub fn as_str(&self) -> Localized<&str> {
        Localized {
            en: self.en.as_str(),
            de: self.de.as_str(),
            fr: self.fr.as_str(),
            it: self.it.as_str(),
            gsw: self.gsw.as_str(),
        }
    }
}

impl<'a, T> From<&'a PartiallyLocalized<T>> for Localized<&'a T> {
    fn from(value: &'a PartiallyLocalized<T>) -> Self {
        Localized {
            en: value.index(Locale::English),
            de: value.index(Locale::German),
            fr: value.index(Locale::French),
            it: value.index(Locale::Italian),
            gsw: value.index(Locale::SwissGerman),
        }
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct PartiallyLocalized<T> {
    pub en: T,
    pub de: Option<T>,
    pub fr: Option<T>,
    pub it: Option<T>,
    pub gsw: Option<T>,
}

impl<T> PartiallyLocalized<T> {
    pub fn get(&self, locale: Locale) -> Option<&T> {
        match locale {
            Locale::English => Some(&self.en),
            Locale::German => self.de.as_ref(),
            Locale::French => self.fr.as_ref(),
            Locale::Italian => self.it.as_ref(),
            Locale::SwissGerman => self.gsw.as_ref(),
        }
    }

    pub fn values(&self) -> Vec<(Locale, &T)> {
        Locale::ALL
            .into_iter()
            .filter_map(|loc| {
                let value = self.get(loc)?;
                Some((loc, value))
            })
            .collect()
    }

    pub fn values_filtered(&self, locale: &Locale) -> Vec<(Locale, &T)> {
        Locale::ALL
            .into_iter()
            .filter_map(|loc| {
                if locale == &loc {
                    return None;
                }
                let value = self.get(loc)?;
                Some((loc, value))
            })
            .collect()
    }
}

impl<T> Index<Locale> for PartiallyLocalized<T> {
    type Output = T;

    fn index(&self, index: Locale) -> &Self::Output {
        match index {
            Locale::English => &self.en,
            Locale::German => self.de.as_ref().unwrap_or(&self.en),
            Locale::French => self.fr.as_ref().or(self.de.as_ref()).unwrap_or(&self.en),
            Locale::Italian => self.it.as_ref().or(self.de.as_ref()).unwrap_or(&self.en),
            Locale::SwissGerman => self.gsw.as_ref().or(self.de.as_ref()).unwrap_or(&self.en),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_locale_parsing() {
        assert_eq!(Locale::from("en"), Locale::English);
        assert_eq!(Locale::from("aa, en"), Locale::English);
        assert_eq!(Locale::from("en-CH"), Locale::English);
        assert_eq!(Locale::from("de"), Locale::German);
        assert_eq!(Locale::from("aa, de"), Locale::German);
        assert_eq!(Locale::from("de-CH"), Locale::German);
        assert_eq!(Locale::from("fr"), Locale::French);
        assert_eq!(Locale::from("aa, fr"), Locale::French);
        assert_eq!(Locale::from("fr-CH"), Locale::French);
        assert_eq!(Locale::from("it"), Locale::Italian);
        assert_eq!(Locale::from("aa, it"), Locale::Italian);
        assert_eq!(Locale::from("it-CH"), Locale::Italian);
        assert_eq!(Locale::from("gsw"), Locale::English);
        assert_eq!(Locale::from("aa, gsw"), Locale::English);
        assert_eq!(Locale::from("gsw-CH"), Locale::English);
        assert_eq!(Locale::from("\t aa , \tbb\n,\nde\t\n"), Locale::German);
        assert_eq!(Locale::from("De"), Locale::German);
        assert_eq!(Locale::from("DE"), Locale::German);
        assert_eq!(Locale::from("dE"), Locale::German);
        assert_eq!(Locale::from("de-EN"), Locale::German);
        assert_eq!(Locale::from("DE-en"), Locale::German);
        assert_eq!(Locale::from(""), Locale::default());
        assert_eq!(Locale::from("aa"), Locale::default());
        assert_eq!(Locale::from("aa, bb, cc"), Locale::default());
    }
}
