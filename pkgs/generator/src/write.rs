use fs_extra::dir::{move_dir, CopyOptions};
use minify_html::{minify, Cfg};
use parser::locale::Locale;
use std::fs::{create_dir_all, write};
use std::path::{Path, PathBuf};
use std::process::Command;
use std::sync::OnceLock;
use temp_dir::TempDir;

pub fn clear_dir() {
    let dir = base_dir();
    if dir.exists() {
        let tmp = TempDir::new()
            .map_err(|e| e.to_string())
            .expect("clear dist directory");
        move_dir(dir, tmp.path(), &CopyOptions::new())
            .map_err(|e| {
                format!(
                    "mv {} to {}. {e}",
                    dir.to_string_lossy(),
                    tmp.path().to_string_lossy()
                )
            })
            .expect("move dist directory to temp directory");
    }
    create_dir(dir);
    for locale in Locale::ALL {
        let base = dir.join(locale.as_slug());
        create_dir(&base);
    }
}

fn create_dir(dir: &Path) {
    create_dir_all(dir)
        .map_err(|e| format!("{e}. {}", dir.to_string_lossy()))
        .expect("create directory");
}

pub fn write_file(file: &Path, data: impl AsRef<[u8]>) {
    let file = base_dir().join(file);
    if let Some(parent) = file.parent() {
        create_dir(parent);
    }
    write(&file, data)
        .map_err(|e| format!("{e}: writing {}", file.to_string_lossy()))
        .expect("write file");
}

pub fn write_html_file(file: &Path, data: String) {
    let data = minify(data.into_bytes().as_slice(), &cfg());
    write_file(file, data);
}

fn base_dir() -> &'static Path {
    static CELL: OnceLock<PathBuf> = OnceLock::new();
    CELL.get_or_init(|| {
        let stdout = Command::new(env!("CARGO"))
            .args(["locate-project", "--workspace", "--message-format=plain"])
            .output()
            .map_err(|e| e.to_string())
            .expect("get output from cargo locate command")
            .stdout;
        std::str::from_utf8(&stdout)
            .map_err(|e| e.to_string())
            .expect("convert bytes to utf8")
            .trim()
            .strip_suffix("Cargo.toml")
            .map(Path::new)
            .expect("there to be a path")
            .join("dist")
    })
}

fn cfg() -> Cfg {
    let mut cfg = Cfg::spec_compliant();
    cfg.keep_closing_tags = true;
    cfg
}
