use std::process::Command;

fn main() {
    println!("cargo::rerun-if-changed=../../templates");
    println!("cargo::rerun-if-changed=../../data");
    println!("cargo::rerun-if-changed=../../static");
    println!("cargo::rerun-if-changed=tailwind.input.css");
    println!("cargo::rerun-if-changed=tailwind.config.js");
    println!("cargo::rerun-if-changed=package.json");

    Command::new("pnpm")
        .arg("install")
        .status()
        .expect("run pnpm install");

    Command::new("pnpm")
        .args(["exec", "tailwindcss"])
        .args(["--config", "tailwind.config.js"])
        .args(["--content", "../../templates/content/**/*.html"])
        .args(["--input", "tailwind.input.css"])
        .args(["--output", "../../templates/content/styles/dist.css"])
        .arg("--minify")
        .status()
        .expect("run tailwind");

    if !cfg!(debug_assertions) {
        Command::new("pnpm")
            .args(["exec", "minify-selectors"])
            .args(["--input", "../../templates/content"])
            .args(["--output", "../../templates/dist"])
            .arg("--parallel")
            .status()
            .expect("run minify-selectors");
    }
}
