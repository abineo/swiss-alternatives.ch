use askama::Template;

use parser::data::{CategoryMap, ProductMap, VendorMap};
use parser::locale::{Locale, Localized};
use parser::translations::Translations;

use crate::meta::MetaData;
use crate::routes::Route;
use crate::templates::shell::Shell;
use crate::write::write_html_file;

#[derive(Template)]
#[template(path = "pages/home.html")]
pub struct Home<'a> {
    pub translations: &'a Translations,
    pub meta: &'a MetaData<'a>,
}

impl<'a> Home<'a> {
    pub fn new(translations: &'a Translations, meta: &'a MetaData) -> Self {
        Self { translations, meta }
    }
}

pub fn render_home(
    translations: &Translations,
    _products: &ProductMap,
    _categories: &CategoryMap,
    _vendors: &VendorMap,
) -> Option<Route> {
    let route = Route::default();
    let canonical = route.to_pathname(None);
    let paths = Localized::new_with(|loc| format!("/{}", loc.as_slug()));

    for locale in Locale::ALL {
        let meta = MetaData {
            locale,
            title: &translations.general.brand[locale],
            description: &translations.general.slogan[locale],
            canonical_path: &canonical,
            paths: &paths.as_ref(),
        };

        let partial = Home::new(translations, &meta)
            .render()
            .expect("render home partial");
        let full = Shell::new(&meta, &partial)
            .render()
            .expect("render home full");

        let base = route.to_pathbuf(locale);
        write_html_file(&base.join("partial.html"), partial);
        write_html_file(&base.join("index.html"), full);
    }

    Some(route)
}
