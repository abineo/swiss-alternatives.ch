fn main() {
    color_eyre::install().expect("setup color-eyre");
    generator::run();
}
