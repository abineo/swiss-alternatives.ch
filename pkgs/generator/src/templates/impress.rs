use askama::Template;

use parser::locale::{Locale, Localized};
use parser::translations::Translations;

use crate::format_title;
use crate::meta::MetaData;
use crate::routes::Route;
use crate::templates::shell::Shell;
use crate::write::write_html_file;

#[derive(Template)]
#[template(path = "pages/impress.html")]
pub struct Impress<'a> {
    pub translations: &'a Translations,
    pub meta: &'a MetaData<'a>,
}

impl<'a> Impress<'a> {
    pub fn new(translations: &'a Translations, meta: &'a MetaData) -> Self {
        Self { translations, meta }
    }
}

pub fn render_impress(translations: &Translations) -> Option<Route> {
    let route = Route {
        canonical: vec![translations.impress.slug[Locale::default()].to_string()],
        localized: Localized::new_with(|loc| vec![translations.impress.slug[loc].to_string()]),
        updated_at: None,
    };
    let canonical = route.to_pathname(None);
    let paths =
        Localized::new_with(|loc| format!("/{}/{}", loc.as_slug(), translations.impress.slug[loc]));

    for locale in Locale::ALL {
        let meta = MetaData {
            locale,
            title: &format_title(vec![
                translations.impress.name[locale].as_str(),
                translations.general.brand[locale].as_str(),
            ]),
            description: &translations.general.slogan[locale],
            canonical_path: &canonical,
            paths: &paths.as_ref(),
        };

        let partial = Impress::new(translations, &meta)
            .render()
            .expect("render impress partial");
        let full = Shell::new(&meta, &partial)
            .render()
            .expect("render impress full");

        let base = route.to_pathbuf(locale);
        write_html_file(&base.join("partial.html"), partial);
        write_html_file(&base.join("index.html"), full);
    }

    Some(route)
}
