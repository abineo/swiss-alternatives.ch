use askama::Template;

use parser::locale::{Locale, Localized};
use parser::translations::Translations;

use crate::format_title;
use crate::meta::MetaData;
use crate::routes::Route;
use crate::templates::shell::Shell;
use crate::write::write_html_file;

#[derive(Template)]
#[template(path = "pages/not_found.html")]
pub struct NotFound<'a> {
    pub translations: &'a Translations,
    pub meta: &'a MetaData<'a>,
}

impl<'a> NotFound<'a> {
    pub fn new(translations: &'a Translations, meta: &'a MetaData) -> Self {
        Self { translations, meta }
    }
}

pub fn render_not_found(translations: &Translations) -> Option<Route> {
    let route = Route::default();
    let canonical = route.to_pathname(None);
    let paths = Localized::new_with(|loc| format!("/{}", loc.as_slug()));

    for locale in Locale::ALL {
        let meta = MetaData {
            locale,
            title: &format_title(vec![
                translations.not_found.name[locale].as_str(),
                translations.general.brand[locale].as_str(),
            ]),
            description: &translations.not_found.text[locale],
            canonical_path: &canonical,
            paths: &paths.as_ref(),
        };

        let partial = NotFound::new(translations, &meta)
            .render()
            .expect("render not_found partial");
        let full = Shell::new(&meta, &partial)
            .render()
            .expect("render not_found full");

        let base = route.to_pathbuf(locale).join("not_found");
        write_html_file(&base.join("partial.html"), partial);
        write_html_file(&base.join("index.html"), full);
    }
    None
}
